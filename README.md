public class Cliente {
    private String cedula;
    private String nombre;
    private String direccion;
    private String correo;   

    public Cliente(String cedula, String nombre, String direccion, String correo) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "Cliente{" + "cedula=" + cedula + ", nombre=" + nombre + ", direccion=" + direccion + ", correo=" + correo + '}';
    }
    
    
}
